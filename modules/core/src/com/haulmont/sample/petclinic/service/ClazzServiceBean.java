package com.haulmont.sample.petclinic.service;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.View;
import com.haulmont.sample.petclinic.HttpResult;
import com.haulmont.sample.petclinic.entity.school.Clazz;
import com.haulmont.sample.petclinic.entity.school.Student;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(ClazzService.NAME)
public class ClazzServiceBean implements ClazzService {

    @Inject
    private Persistence persistence;

    @Override
    public HttpResult getClazzByName(String name) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Clazz clazz = (Clazz) em.createQuery("SELECT u FROM petclinic_Clazz u WHERE u.name = :name")
                    .setParameter("name", name)
                    .setView(Clazz.class, View.LOCAL)
                    .getFirstResult();
            tx.commit();
            return HttpResult.builder().setSuccess(true).setData(clazz).build();
        }
    }

    @Override
    public HttpResult saveClazz(Clazz clazz) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            em.persist(clazz);
            tx.commit();
            return HttpResult.builder().setSuccess(true).build();
        }
    }

    @Override
    public HttpResult getStudentByClazz(String clazzName) {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            List<Student> students = em.createQuery(
                    "SELECT u FROM petclinic_Student u WHERE u.clazz.name = :name", Student.class)
                    .setParameter("name", clazzName)
                    .setView(Student.class, "student-clazz-view")
                    .getResultList();
            tx.commit();
            return HttpResult.builder().setSuccess(true).setData(students).build();
        }
    }
}
