package com.haulmont.sample.petclinic.service;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.sample.petclinic.entity.Area;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(AreaService.NAME)
public class AreaServiceBean implements AreaService {

    @Inject
    private DataManager dataManager;

    @Override
    public List<Area> findAreaByParentCode(Integer code) {
        return dataManager.load(Area.class)
                .query("select c from petclinic_Area c where c.parentCode = :code order by c.code")
                .parameter("code", code)
                .view("_local")
                .list();
    }

    @Override
    public Area getValueByCode(Integer code) {
        return dataManager.load(Area.class)
                .query("select c from petclinic_Area c where c.code = :code")
                .parameter("code", code)
                .view("_local")
                .one();
    }
}