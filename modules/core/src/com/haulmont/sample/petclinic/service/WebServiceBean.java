package com.haulmont.sample.petclinic.service;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.sample.petclinic.entity.school.Clazz;
import com.haulmont.sample.petclinic.vo.BaseVO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(WebService.NAME)
public class WebServiceBean implements WebService {

    @Inject
    private DataManager dataManager;

    @Override
    public List<Clazz> getClazzList() {
        return dataManager.load(Clazz.class).view("clazz-view").list();
    }

    @Override
    public Clazz getClazzByName(String name) {
        return dataManager.load(Clazz.class)
                .query("select c from petclinic_Clazz c where c.name = :name")
                .parameter("name", name)
                .view("clazz-view")
                .one();
    }

}
