package com.haulmont.sample.petclinic.service;

import com.haulmont.sample.petclinic.core.StudentBean;
import com.haulmont.sample.petclinic.vo.BaseVO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service(StudentService.NAME)
public class StudentServiceBean implements StudentService {

    @Inject
    private StudentBean studentBean;

    @Override
    public List<BaseVO> loadStudentByAge() {
        return studentBean.getStudentForAge();
    }

    @Override
    public List<BaseVO> loadStudentByHobby() {
        return studentBean.getStudentForHobby();
    }

    @Override
    public List<BaseVO> loadStudentByClazz() {
        return studentBean.getStudentForClazz();
    }

}
