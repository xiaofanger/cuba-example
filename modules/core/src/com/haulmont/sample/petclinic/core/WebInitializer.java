package com.haulmont.sample.petclinic.core;

import com.haulmont.cuba.core.sys.servlet.ServletRegistrationManager;
import com.haulmont.cuba.core.sys.servlet.events.ServletContextInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.Servlet;
import javax.servlet.ServletRegistration;

/**
 * @author LiFan
 * @date 2020/4/10 10:26
 */
@Component
public class WebInitializer {

    @Inject
    private ServletRegistrationManager servletRegistrationManager;

    @EventListener
    public void initializeHttpServlet(ServletContextInitializedEvent e) {
        Servlet servlet = servletRegistrationManager.createServlet(
                e.getApplicationContext(), "com.alibaba.druid.support.http.StatViewServlet");
        ServletRegistration.Dynamic servletRegistration = e.getSource().addServlet("druidStatView", servlet);
        servletRegistration.setInitParameter("resetEnable", "true");
        servletRegistration.setInitParameter("loginUsername", "");
        servletRegistration.setInitParameter("loginPassword", "");
        servletRegistration.addMapping("/druid/*");

//        Filter filter = servletRegistrationManager.createFilter(e.getApplicationContext(), "com.alibaba.druid.support.http.WebStatFilter");
//        FilterRegistration.Dynamic filterRegistration = e.getSource().addFilter("druidWebStatFilter", filter);
//        filterRegistration.addMappingForServletNames();
//        filterRegistration.addMappingForUrlPatterns();
    }
}
