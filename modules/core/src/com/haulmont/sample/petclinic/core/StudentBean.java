package com.haulmont.sample.petclinic.core;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.sample.petclinic.vo.BaseVO;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component(StudentBean.NAME)
public class StudentBean {
    public static final String NAME = "petclinic_StudentBean";

    @Inject
    private Persistence persistence;

    public List<BaseVO> getStudentForAge() {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query = em.createQuery(
                    "select s.age, count(s.age) from petclinic_Student s group by s.age");
            List list = query.getResultList();
            List<BaseVO> vos = new ArrayList<>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                Integer age = (Integer) row[0];
                Long amount = (Long) row[1];
                vos.add(BaseVO.builder().age(age).amount(amount).build());
            }
            tx.commit();
            return vos;
        }
    }

    public List<BaseVO> getStudentForHobby() {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query = em.createQuery(
                    "select s.hobby, count(s.hobby) from petclinic_Student s group by s.hobby");
            List list = query.getResultList();
            List<BaseVO> vos = new ArrayList<>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                String hobby = (String) row[0];
                Long amount = (Long) row[1];
                vos.add(BaseVO.builder().hobby(hobby).amount(amount).build());
            }
            tx.commit();
            return vos;
        }
    }

    public List<BaseVO> getStudentForClazz() {
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query = em.createQuery(
                    "select s.clazz.name, count(s.clazz.name) from petclinic_Student s group by s.clazz.name");
            List list = query.getResultList();
            List<BaseVO> vos = new ArrayList<>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                String clazz = (String) row[0];
                Long amount = (Long) row[1];
                vos.add(BaseVO.builder().clazz(clazz).amount(amount).build());
            }
            tx.commit();
            return vos;
        }
    }

}
