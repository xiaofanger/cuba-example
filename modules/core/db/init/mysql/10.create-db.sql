-- begin PETCLINIC_CLAZZ
create table PETCLINIC_CLAZZ (
    ID integer,
    UUID varchar(32),
    --
    NAME varchar(255),
    TOTAL integer,
    TEACHER varchar(255),
    --
    primary key (ID)
)^
-- end PETCLINIC_CLAZZ
-- begin PETCLINIC_SCHOOL
create table PETCLINIC_SCHOOL (
    ID integer,
    UUID varchar(32),
    --
    CODE varchar(255),
    NAME varchar(255),
    ADDRESS varchar(255),
    COUNTRY varchar(50),
    TELEPHONE varchar(255),
    CREATED_DATE date,
    --
    primary key (ID)
)^
-- end PETCLINIC_SCHOOL
-- begin PETCLINIC_STUDENT
create table PETCLINIC_STUDENT (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    AGE integer,
    BIRTHDAY date,
    HOBBY varchar(255),
    SEX varchar(50),
    CLAZZ_ID integer,
    --
    primary key (ID)
)^
-- end PETCLINIC_STUDENT
-- begin PETCLINIC_CITY
create table PETCLINIC_CITY (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    CODE varchar(255),
    POSTCODE varchar(255),
    DESCRIPTION varchar(255),
    --
    primary key (ID)
)^
-- end PETCLINIC_CITY
-- begin PETCLINIC_SUBJECT
create table PETCLINIC_SUBJECT (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    TEACHER varchar(255),
    TERM integer,
    --
    primary key (ID)
)^
-- end PETCLINIC_SUBJECT
-- begin PETCLINIC_CITY_STUDENT_LINK
create table PETCLINIC_CITY_STUDENT_LINK (
    CITY_ID varchar(32),
    STUDENT_ID varchar(32),
    primary key (CITY_ID, STUDENT_ID)
)^
-- end PETCLINIC_CITY_STUDENT_LINK
-- begin PETCLINIC_SUBJECT_STUDENT_LINK
create table PETCLINIC_SUBJECT_STUDENT_LINK (
    SUBJECT_ID varchar(32),
    STUDENT_ID varchar(32),
    primary key (SUBJECT_ID, STUDENT_ID)
)^
-- end PETCLINIC_SUBJECT_STUDENT_LINK
-- begin PETCLINIC_AREA
create table PETCLINIC_AREA (
    ID varchar(32),
    VERSION integer not null,
    CREATE_TS datetime(3),
    CREATED_BY varchar(50),
    UPDATE_TS datetime(3),
    UPDATED_BY varchar(50),
    DELETE_TS datetime(3),
    DELETED_BY varchar(50),
    --
    CODE integer,
    VALUE varchar(255),
    PARENT_CODE integer,
    --
    primary key (ID)
)^
-- end PETCLINIC_AREA
