package com.haulmont.sample.petclinic.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LiFan
 * @date 2019/12/9 12:52
 */
@Data
@Builder
public class BaseVO implements Serializable {
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 人数
     */
    private Long amount;
    /**
     * 爱好
     */
    private String hobby;
    /**
     * 班级
     */
    private String clazz;
    /**
     * 学生
     */
    private String student;

}
