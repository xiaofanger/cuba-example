package com.haulmont.sample.petclinic.service;

import com.haulmont.sample.petclinic.entity.Area;

import java.util.List;

public interface AreaService {
    String NAME = "petclinic_AreaService";

    List<Area> findAreaByParentCode(Integer code);

    Area getValueByCode(Integer code);

}