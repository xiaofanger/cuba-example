package com.haulmont.sample.petclinic.service;

import com.haulmont.sample.petclinic.HttpResult;
import com.haulmont.sample.petclinic.entity.school.Clazz;

public interface ClazzService {
    String NAME = "petclinic_ClazzService";

    /**
     *
     * @param name 班级名称
     * @return
     */
    HttpResult getClazzByName(String name);

    /**
     * 保存一条记录
     *
     * @param clazz 实体对象
     * @return
     */
    HttpResult saveClazz(Clazz clazz);

    /**
     *
     * @param clazzName
     * @return
     */
    HttpResult getStudentByClazz(String clazzName);
}
