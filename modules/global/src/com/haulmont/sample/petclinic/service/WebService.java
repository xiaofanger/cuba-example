package com.haulmont.sample.petclinic.service;

import com.haulmont.sample.petclinic.entity.school.Clazz;
import com.haulmont.sample.petclinic.vo.BaseVO;

import java.util.List;

public interface WebService {

    String NAME = "petclinic$WebService";

    /**
     * 加载所有班级信息
     * @return
     */
    List<Clazz> getClazzList();

    /**
     *
     * @param name
     * @return
     */
    Clazz getClazzByName(String name);

}
