package com.haulmont.sample.petclinic.service;

import com.haulmont.sample.petclinic.vo.BaseVO;

import java.util.List;

public interface StudentService {
    String NAME = "petclinic_StudentService";

    List<BaseVO> loadStudentByAge();

    List<BaseVO> loadStudentByHobby();

    List<BaseVO> loadStudentByClazz();

}
