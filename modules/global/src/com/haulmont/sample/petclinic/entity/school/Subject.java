package com.haulmont.sample.petclinic.entity.school;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.util.Set;

@NamePattern("%s|name")
@Table(name = "PETCLINIC_SUBJECT")
@Entity(name = "petclinic_Subject")
public class Subject extends StandardEntity {
    private static final long serialVersionUID = 2516610148606420525L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "TEACHER")
    protected String teacher;

    @Column(name = "TERM")
    protected Integer term;

    @JoinTable(name = "PETCLINIC_SUBJECT_STUDENT_LINK",
            joinColumns = @JoinColumn(name = "SUBJECT_ID"),
            inverseJoinColumns = @JoinColumn(name = "STUDENT_ID"))
    @ManyToMany
    protected Set<Student> students;

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
