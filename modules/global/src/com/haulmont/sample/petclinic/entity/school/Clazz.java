package com.haulmont.sample.petclinic.entity.school;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@NamePattern("%s|name")
@Table(name = "PETCLINIC_CLAZZ")
@Entity(name = "petclinic_Clazz")
public class Clazz extends BaseIntegerIdEntity implements HasUuid {
    private static final long serialVersionUID = 1035412261880317583L;

    @Column(name = "UUID")
    protected UUID uuid;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "TOTAL")
    protected Integer total;

    @Column(name = "TEACHER")
    protected String teacher;

    @OneToMany(mappedBy = "clazz")
    protected List<Student> students;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
