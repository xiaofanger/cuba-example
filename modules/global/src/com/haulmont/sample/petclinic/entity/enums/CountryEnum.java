package com.haulmont.sample.petclinic.entity.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum CountryEnum implements EnumClass<String> {

    CHAIN("中国"),
    UK("英国"),
    USA("美国"),
    CANADA("加拿大");

    private String id;

    CountryEnum(String value) {
        this.id = value;
    }

    @Override
    public String getId() {
        return id;
    }

    @Nullable
    public static CountryEnum fromId(String id) {
        for (CountryEnum at : CountryEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
