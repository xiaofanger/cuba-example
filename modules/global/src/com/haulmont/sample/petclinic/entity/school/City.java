package com.haulmont.sample.petclinic.entity.school;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import java.util.List;

@NamePattern("%s|name")
@Table(name = "PETCLINIC_CITY")
@Entity(name = "petclinic_City")
public class City extends StandardEntity {
    private static final long serialVersionUID = 2613664934155445126L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "CODE")
    protected String code;

    @Column(name = "POSTCODE")
    protected String postcode;

    @Column(name = "DESCRIPTION")
    protected String description;

    @JoinTable(name = "PETCLINIC_CITY_STUDENT_LINK",
            joinColumns = @JoinColumn(name = "CITY_ID"),
            inverseJoinColumns = @JoinColumn(name = "STUDENT_ID"))
    @ManyToMany
    protected List<Student> students;

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
