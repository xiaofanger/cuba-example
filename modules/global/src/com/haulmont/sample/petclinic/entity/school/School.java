package com.haulmont.sample.petclinic.entity.school;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.sample.petclinic.entity.enums.CountryEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@NamePattern("%s|name")
@Table(name = "PETCLINIC_SCHOOL", indexes = {
        @Index(name = "IDX_PETCLINIC_SCHOOL", columnList = "NAME")
})
@Entity(name = "petclinic_School")
public class School extends BaseIntegerIdEntity implements HasUuid {
    private static final long serialVersionUID = -7049963284218813257L;

    @Column(name = "UUID")
    protected UUID uuid;

    @Column(name = "CODE")
    protected String code;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "ADDRESS")
    protected String address;

    @Column(name = "COUNTRY")
    protected String country;

    @Column(name = "TELEPHONE")
    protected String telephone;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATED_DATE")
    protected Date createdDate;

    public CountryEnum getCountry() {
        return country == null ? null : CountryEnum.fromId(country);
    }

    public void setCountry(CountryEnum country) {
        this.country = country == null ? null : country.getId();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
