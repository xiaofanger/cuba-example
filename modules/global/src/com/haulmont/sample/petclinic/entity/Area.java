package com.haulmont.sample.petclinic.entity;

import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Table(name = "PETCLINIC_AREA")
@Entity(name = "petclinic_Area")
public class Area extends StandardEntity {
    private static final long serialVersionUID = -5843168218239571882L;

    @Column(name = "CODE")
    protected Integer code;

    @Column(name = "VALUE")
    protected String value;

    @Column(name = "PARENT_CODE")
    protected Integer parentCode;

    public void setParentCode(Integer parentCode) {
        this.parentCode = parentCode;
    }

    public Integer getParentCode() {
        return parentCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}