package com.haulmont.sample.petclinic.entity.enums;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;


public enum SexEnum implements EnumClass<String> {

    MAN("男"),
    WOMAN("女");

    private String id;

    SexEnum(String value) {
        this.id = value;
    }

    @Override
    public String getId() {
        return id;
    }

    @Nullable
    public static SexEnum fromId(String id) {
        for (SexEnum at : SexEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}
