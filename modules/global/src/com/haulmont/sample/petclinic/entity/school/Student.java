package com.haulmont.sample.petclinic.entity.school;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.sample.petclinic.entity.enums.SexEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NamePattern("%s|name")
@Table(name = "PETCLINIC_STUDENT")
@Entity(name = "petclinic_Student")
public class Student extends StandardEntity {
    private static final long serialVersionUID = -2591081922144760187L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "AGE")
    protected Integer age;

    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTHDAY")
    protected Date birthday;

    @Column(name = "HOBBY")
    protected String hobby;

    @Column(name = "SEX")
    protected String sex;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLAZZ_ID")
    protected Clazz clazz;

    @JoinTable(name = "PETCLINIC_SUBJECT_STUDENT_LINK",
            joinColumns = @JoinColumn(name = "STUDENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "SUBJECT_ID"))
    @ManyToMany
    protected Set<Subject> subjects;

    @JoinTable(name = "PETCLINIC_CITY_STUDENT_LINK",
            joinColumns = @JoinColumn(name = "STUDENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "CITY_ID"))
    @ManyToMany
    protected List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Clazz getClazz() {
        return clazz;
    }

    public void setClazz(Clazz clazz) {
        this.clazz = clazz;
    }

    public SexEnum getSex() {
        return sex == null ? null : SexEnum.fromId(sex);
    }

    public void setSex(SexEnum sex) {
        this.sex = sex == null ? null : sex.getId();
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
