package com.haulmont.sample.petclinic;

import java.io.Serializable;

/**
 * @author LiFan
 * @date 2019/12/3 13:07
 */
public class HttpResult implements Serializable {

    private boolean success;

    private String errorMessage;

    private Object data;

    public HttpResult(boolean success, String errorMessage, Object data) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public static HttpResult.HttpResultBuilder builder() {
        return new HttpResult.HttpResultBuilder();
    }

    public static class HttpResultBuilder {
        private boolean success;
        private String errorMessage;
        private Object data;

        public HttpResult.HttpResultBuilder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public HttpResult.HttpResultBuilder setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
            return this;
        }

        public HttpResult.HttpResultBuilder setData(Object data) {
            this.data = data;
            return this;
        }

        public HttpResult build() {
            return new HttpResult(this.success, this.errorMessage, this.data);
        }

    }


}
