import * as React from "react";
import {observer} from "mobx-react";
import {collection} from "@cuba-platform/react";
import {Student} from "../../cuba/entities/petclinic_Student";

@observer
class BookPage extends React.Component {
  carsData = collection<Student>(Student.NAME, {view: 'student-clazz-subject-city-view'});
  render() {
    if (this.carsData.status === "LOADING") return 'Loading...';
    return (
      <ul>
        {this.carsData.items.map(car =>
          <li key={car.id}>{car._instanceName}</li>
        )}
      </ul>
    )
  }
}

export default BookPage;
