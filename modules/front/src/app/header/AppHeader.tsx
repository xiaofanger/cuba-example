import {Button, Icon, Menu, Modal} from "antd";
import * as React from "react";
import {observer} from "mobx-react";
import './AppHeader.css';
import logo from './logo.png';
import {injectMainStore, MainStoreInjected} from "@cuba-platform/react";

@injectMainStore
@observer
class AppHeader extends React.Component<MainStoreInjected> {
  state = {
    current: 'mail',
  };

  render() {
    const appState = this.props.mainStore!;

    return (
      <div className="AppHeader">
        <div>
          <img src={logo} alt={'Logo'}/>
        </div>
        {/*<div style={{flex: 1}}>*/}
        {/*  <Menu selectedKeys={[this.state.current]} mode="horizontal" style={{height: 64}}>*/}
        {/*    <Menu.Item key="mail">*/}
        {/*      <Icon type="mail" />*/}
        {/*      Navigation One*/}
        {/*    </Menu.Item>*/}
        {/*  </Menu>*/}
        {/*</div>*/}
        <div className="user-info">
          <span>{appState.userName}</span>
          <Button ghost={true}
                  icon='logout'
                  style={{border: 0}}
                  onClick={this.showLogoutConfirm}/>
        </div>
      </div>
    )
  }

  showLogoutConfirm = () => {
    Modal.confirm({
      title: 'Are you sure you want to logout?',
      okText: 'Logout',
      cancelText: 'Cancel',
      onOk: () => {
        this.props.mainStore!.logout()
      }
    });
  }

}

export default AppHeader;
