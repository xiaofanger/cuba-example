import * as React from "react";
import {Button, Table, Divider, Tag, Modal, Input, Icon} from "antd";
import {
  collection,
  DataTable,
  EntityProperty, FormField,
  injectMainStore,
  instance, MainStoreInjected,
} from "@cuba-platform/react";
import {Student} from "../../cuba/entities/petclinic_Student";
import {observer} from "mobx-react";
import Form from "antd/es/form";
import {EnumInfo} from "@cuba-platform/rest";
import {CubaRest} from "../../cuba-rest";



@injectMainStore
@observer
class StudentPage extends React.Component<MainStoreInjected> {

  state = {
    visible: false,
    selectId: ''
  };

  private dataCollection = collection<Student>(Student.NAME, {view: 'student-clazz-subject-city-view', loadImmediately: true});
  private dataInstance = instance<Student>(Student.NAME, {view: '_local', loadImmediately: false});

  private fields = ['name', 'age', 'birthday', 'hobby', 'sex'];

  columns = [{
    title: '学生姓名',
    dataIndex: 'name',
  }, {
    title: '年龄',
    dataIndex: 'age',
  }, {
    title: '生日',
    dataIndex: 'birthday',
  }, {
    title: '兴趣爱好',
    dataIndex: 'hobby',
  }, {
    title: '性别',
    dataIndex: 'sex',
  }];

  handleCreateBtnClick = () => {
    console.log('clicked button!');
  };

  handleEditBtnClick = () => {
    // console.log(this.dataSource());
    // this.showModal();
    CubaRest.restApi().loadEnums()
      .then(((enums: EnumInfo[]) => {
        console.log('enums', enums)
      }));
    CubaRest.restApi().loadEntities(Student.NAME).then((users)=> {
      console.log('students', users)
    })
  };

  handleRemoveBtnClick = () => {
    if (this.state.selectId == null) {
      return;
    }
    console.log(this.dataInstance)
    this.dataInstance.load(this.state.selectId);
    console.log(this.dataInstance)
  };

  showModal = () => {
    if (this.state.selectId == null) {
      return;
    }


    // 显示Dialog
    this.setState({visible: true,});
  };

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };



  dataSource = () => {
    return this.dataCollection.items.map((item) => {
      return {
        key: item.id,
        ...item
      }
    })
  }


  private tableProps = {
    bordered: true,
    columns: [{
      title: '学生姓名',
      dataIndex: 'name',
    }, {
      title: '年龄',
      dataIndex: 'age',
    }, {
      title: '生日',
      dataIndex: 'birthday',
    }, {
      title: '兴趣爱好',
      dataIndex: 'hobby',
    }, {
      title: '性别',
      dataIndex: 'sex',
    }]
  }

  onSelectedRowChange = (selectedRowKey: string | number | undefined) => {
      this.setState({selectId: selectedRowKey})
  };

  render() {
    return(
      <div>
        <Table dataSource={this.dataSource()} columns={this.columns} bordered={true} />
        <DataTable dataCollection={this.dataCollection}
                   fields={this.fields}
                   defaultSort={'-updateTs'}
                   tableProps={this.tableProps}
                   buttons={[
                     <Button type="primary" icon={"plus"} onClick={this.handleCreateBtnClick}>新建</Button>,
                     <Button type="dashed" icon={"edit"} onClick={this.handleEditBtnClick}>编辑</Button>,
                     <Button type="danger" icon={"delete"} onClick={this.handleRemoveBtnClick}>删除</Button>
                   ]}
                   onSelectedRowChange={this.onSelectedRowChange}
        />
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form layout='vertical' >
            <Form.Item label='学生姓名'>
              <FormField entityName={Student.NAME} propertyName='name' optionsContainer={this.dataCollection}/>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    )
  }
}

export default StudentPage;

