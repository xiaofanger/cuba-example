export enum SexEnum {
    MAN = "MAN",
    WOMAN = "WOMAN"
}

export enum CountryEnum {
    CHAIN = "CHAIN",
    UK = "UK",
    USA = "USA",
    CANADA = "CANADA"
}

