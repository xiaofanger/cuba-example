import { StandardEntity } from "./base/sys$StandardEntity";
import { Student } from "./petclinic_Student";
export class Subject extends StandardEntity {
    static NAME = "petclinic_Subject";
    name?: string | null;
    teacher?: string | null;
    term?: number | null;
    students?: Student[] | null;
}
export type SubjectViewName = "_minimal" | "_local" | "_base" | "subject-student-view";
export type SubjectView<V extends SubjectViewName> = V extends "_minimal" ? Pick<Subject, "id" | "name"> : V extends "_local" ? Pick<Subject, "id" | "name" | "teacher" | "term"> : V extends "_base" ? Pick<Subject, "id" | "name" | "teacher" | "term"> : V extends "subject-student-view" ? Pick<Subject, "id" | "name" | "teacher" | "term" | "students"> : never;
