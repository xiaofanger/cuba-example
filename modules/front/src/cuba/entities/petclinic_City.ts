import { StandardEntity } from "./base/sys$StandardEntity";
import { Student } from "./petclinic_Student";
export class City extends StandardEntity {
    static NAME = "petclinic_City";
    name?: string | null;
    code?: string | null;
    postcode?: string | null;
    description?: string | null;
    students?: Student[] | null;
}
export type CityViewName = "_minimal" | "_local" | "_base";
export type CityView<V extends CityViewName> = V extends "_minimal" ? Pick<City, "id" | "name"> : V extends "_local" ? Pick<City, "id" | "name" | "code" | "postcode" | "description"> : V extends "_base" ? Pick<City, "id" | "name" | "code" | "postcode" | "description"> : never;
