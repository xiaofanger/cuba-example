import { BaseIntegerIdEntity } from "./base/sys$BaseIntegerIdEntity";
import { Student } from "./petclinic_Student";
export class Clazz extends BaseIntegerIdEntity {
    static NAME = "petclinic_Clazz";
    uuid?: any | null;
    name?: string | null;
    total?: number | null;
    teacher?: string | null;
    students?: Student[] | null;
}
export type ClazzViewName = "_minimal" | "_local" | "_base" | "clazz-view";
export type ClazzView<V extends ClazzViewName> = V extends "_minimal" ? Pick<Clazz, "id" | "name"> : V extends "_local" ? Pick<Clazz, "id" | "name" | "total" | "teacher"> : V extends "_base" ? Pick<Clazz, "id" | "name" | "total" | "teacher"> : V extends "clazz-view" ? Pick<Clazz, "id" | "name" | "total" | "teacher" | "students"> : never;
