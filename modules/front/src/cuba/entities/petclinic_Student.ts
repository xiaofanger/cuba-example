import { StandardEntity } from "./base/sys$StandardEntity";
import { SexEnum } from "../enums/enums";
import { Clazz } from "./petclinic_Clazz";
import { Subject } from "./petclinic_Subject";
import { City } from "./petclinic_City";
export class Student extends StandardEntity {
    static NAME = "petclinic_Student";
    name?: string | null;
    age?: number | null;
    birthday?: any | null;
    hobby?: string | null;
    sex?: SexEnum | null;
    clazz?: Clazz | null;
    subjects?: Subject[] | null;
    cities?: City[] | null;
}
export type StudentViewName = "_minimal" | "_local" | "_base" | "student-clazz-view" | "student-clazz-subject-city-view";
export type StudentView<V extends StudentViewName> = V extends "_minimal" ? Pick<Student, "id" | "name"> : V extends "_local" ? Pick<Student, "id" | "name" | "age" | "birthday" | "hobby" | "sex"> : V extends "_base" ? Pick<Student, "id" | "name" | "age" | "birthday" | "hobby" | "sex"> : V extends "student-clazz-view" ? Pick<Student, "id" | "name" | "age" | "birthday" | "hobby" | "sex" | "clazz"> : V extends "student-clazz-subject-city-view" ? Pick<Student, "id" | "name" | "age" | "birthday" | "hobby" | "sex" | "clazz" | "subjects" | "cities"> : never;
