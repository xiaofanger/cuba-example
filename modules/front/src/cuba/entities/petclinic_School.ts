import { BaseIntegerIdEntity } from "./base/sys$BaseIntegerIdEntity";
import { CountryEnum } from "../enums/enums";
export class School extends BaseIntegerIdEntity {
    static NAME = "petclinic_School";
    uuid?: any | null;
    code?: string | null;
    name?: string | null;
    address?: string | null;
    country?: CountryEnum | null;
    telephone?: string | null;
    createdDate?: any | null;
}
export type SchoolViewName = "_minimal" | "_local" | "_base";
export type SchoolView<V extends SchoolViewName> = V extends "_minimal" ? Pick<School, "id" | "name"> : V extends "_local" ? Pick<School, "id" | "code" | "name" | "address" | "country" | "telephone" | "createdDate"> : V extends "_base" ? Pick<School, "id" | "name" | "code" | "address" | "country" | "telephone" | "createdDate"> : never;
