import { CubaApp, FetchOptions } from "@cuba-platform/rest";

export type petclinic_ClazzService_getStudentByClazz_params = {
    clazzName: any;
};

export type petclinic_ClazzService_getClazzByName_params = {
    name: any;
};

export type petclinic_ClazzService_saveClazz_params = {
    clazz: any;
};

export type petclinic$WebService_getClazzByName_params = {
    name: any;
};

export var restServices = {
    petclinic_ClazzService: {
        getStudentByClazz: (cubaApp: CubaApp, fetchOpts?: FetchOptions) => (params: petclinic_ClazzService_getStudentByClazz_params) => {
            return cubaApp.invokeService("petclinic_ClazzService", "getStudentByClazz", params, fetchOpts);
        },
        getClazzByName: (cubaApp: CubaApp, fetchOpts?: FetchOptions) => (params: petclinic_ClazzService_getClazzByName_params) => {
            return cubaApp.invokeService("petclinic_ClazzService", "getClazzByName", params, fetchOpts);
        },
        saveClazz: (cubaApp: CubaApp, fetchOpts?: FetchOptions) => (params: petclinic_ClazzService_saveClazz_params) => {
            return cubaApp.invokeService("petclinic_ClazzService", "saveClazz", params, fetchOpts);
        }
    },
    petclinic$WebService: {
        getClazzList: (cubaApp: CubaApp, fetchOpts?: FetchOptions) => () => {
            return cubaApp.invokeService("petclinic$WebService", "getClazzList", {}, fetchOpts);
        },
        getClazzByName: (cubaApp: CubaApp, fetchOpts?: FetchOptions) => (params: petclinic$WebService_getClazzByName_params) => {
            return cubaApp.invokeService("petclinic$WebService", "getClazzByName", params, fetchOpts);
        }
    }
};

