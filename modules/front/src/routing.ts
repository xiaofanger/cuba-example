import BookPage from "./app/book/BookPage";
import StudentPage from "./app/student/StudentPage";
import {AppInfo} from "./app/info/AppInfo";

export interface RouteInfo {
  pathPattern: string;
  menuLink: string;
  component: any;
  caption: string;
}

export const mainRoutes: RouteInfo[] = [];

mainRoutes.push(
  {
    pathPattern: '/app-info',
    menuLink: '/app-info',
    component: AppInfo,
    caption: '应用信息'
  }, {
    pathPattern: '/book',
    menuLink: '/book',
    component: BookPage,
    caption: '书包'
  }, {
    pathPattern: '/student',
    menuLink: '/student',
    component: StudentPage,
    caption: '学生'
  }
);
