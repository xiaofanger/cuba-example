import {CubaApp, initializeApp} from "@cuba-platform/rest";

export class CubaRest {

  static cubaREST: CubaApp;

  static restApi = (): CubaApp => {
    if (!CubaRest.cubaREST) {
      CubaRest.cubaREST = initializeApp({
        name: 'myApp',
        apiUrl: 'http://localhost:8080/petclinic/rest/'
      })
    }
    return CubaRest.cubaREST;
  }
}

