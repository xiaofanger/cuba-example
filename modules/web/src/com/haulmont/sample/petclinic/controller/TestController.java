package com.haulmont.sample.petclinic.controller;

import com.haulmont.sample.petclinic.entity.school.Student;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LiFan
 * @date 2020/4/10 10:16
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/getObj")
    public Object getTestObject() {
        return "This is test method";
    }

}
