package com.haulmont.sample.petclinic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author LiFan
 * @date 2020/4/10 10:21
 */
@Controller
public class DruidController {

    @RequestMapping("/druid")
    public String index() {
        return "druid/index.html";
    }

}
