package com.haulmont.sample.petclinic.web.screens.clazz;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Clazz;

@UiController("petclinic_Clazz.edit")
@UiDescriptor("clazz-edit.xml")
@EditedEntityContainer("clazzDc")
@LoadDataBeforeShow
public class ClazzEdit extends StandardEditor<Clazz> {
}
