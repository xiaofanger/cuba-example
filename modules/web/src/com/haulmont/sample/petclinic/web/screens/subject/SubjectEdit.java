package com.haulmont.sample.petclinic.web.screens.subject;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Subject;

@UiController("petclinic_Subject.edit")
@UiDescriptor("subject-edit.xml")
@EditedEntityContainer("subjectDc")
@LoadDataBeforeShow
public class SubjectEdit extends StandardEditor<Subject> {
}
