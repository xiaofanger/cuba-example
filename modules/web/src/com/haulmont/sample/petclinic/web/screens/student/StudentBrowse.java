package com.haulmont.sample.petclinic.web.screens.student;

import com.haulmont.charts.gui.components.charts.PieChart;
import com.haulmont.charts.gui.components.charts.SerialChart;
import com.haulmont.charts.gui.data.ListDataProvider;
import com.haulmont.charts.gui.data.MapDataItem;
import com.haulmont.cuba.gui.Route;
import com.haulmont.cuba.gui.components.TabSheet;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Student;
import com.haulmont.sample.petclinic.service.StudentService;
import com.haulmont.sample.petclinic.vo.BaseVO;

import javax.inject.Inject;
import java.util.List;

@UiController("petclinic_Student.browse")
@UiDescriptor("student-browse.xml")
@LookupComponent("studentsTable")
@LoadDataBeforeShow
@Route("student")
public class StudentBrowse extends StandardLookup<Student> {

    @Subscribe("tabSheet")
    public void onTabSheetSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
        String name = event.getSelectedTab().getName();
        switch (name) {
            case "tab1":
                break;
            case "tab2":
                loadPieChartData();
                loadSerialChartData();
                loadSerialChartSourceData();
                break;
            default:
                break;
        }
    }

    @Inject
    private StudentService studentService;
    @Inject
    private PieChart pieChart;
    @Inject
    private SerialChart serialChart;
    @Inject
    private SerialChart serialChartSource;
    @Inject
    private CollectionContainer<Student> studentsDc;

    private void loadPieChartData() {
        ListDataProvider dataProvider = new ListDataProvider();
        // 初始化数据组件
        if (pieChart.getDataProvider() == null) {
            pieChart.setDataProvider(dataProvider);
        }
        // 清空数据
        if (pieChart.getDataProvider().getItems().size() > 0) {
            pieChart.getDataProvider().removeAll();
        }
        // 重新加载数据
        List<BaseVO> students = studentService.loadStudentByAge();
        for (BaseVO vo : students) {
            pieChart.getDataProvider().addItem(MapDataItem.of("key", vo.getAge()+"岁",
                    "value", vo.getAmount()));
        }
    }

    private void loadSerialChartData() {
        ListDataProvider dataProvider = new ListDataProvider();
        // 初始化数据组件
        if (serialChart.getDataProvider() == null) {
            serialChart.setDataProvider(dataProvider);
        }
        // 清空数据
        if (serialChart.getDataProvider().getItems().size() > 0) {
            serialChart.getDataProvider().removeAll();
        }
        // 重新加载数据
        List<BaseVO> students = studentService.loadStudentByHobby();
        for (BaseVO vo : students) {
            serialChart.getDataProvider().addItem(MapDataItem.of("key", vo.getHobby(),
                    "value", vo.getAmount()));
        }
    }

    private void loadSerialChartSourceData() {
        ListDataProvider dataProvider = new ListDataProvider();
        // 初始化数据组件
        if (serialChartSource.getDataProvider() == null) {
            serialChartSource.setDataProvider(dataProvider);
        }
        // 清空数据
        if (serialChartSource.getDataProvider().getItems().size() > 0) {
            serialChartSource.getDataProvider().removeAll();
        }
        // 重新加载数据
        List<Student> students = studentsDc.getItems();
        for (Student vo : students) {
            serialChartSource.getDataProvider().addItem(MapDataItem.of("key", vo.getName(),
                    "value", vo.getSubjects().size()));
        }
    }

}
