package com.haulmont.sample.petclinic.web.screens.clazz;

import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Clazz;
import com.haulmont.sample.petclinic.entity.school.Student;

import javax.inject.Inject;
import java.util.Set;

@UiController("petclinic_Clazz.browse")
@UiDescriptor("clazz-browse.xml")
@LookupComponent("clazzesTable")
@LoadDataBeforeShow
public class ClazzBrowse extends StandardLookup<Clazz> {

    @Inject
    private CollectionLoader<Student> studentsDl;
    private Integer clazz;

    public void setClazz(Integer clazz) {
        this.clazz = clazz;
    }

    @Subscribe
    private void onBeforeShow(BeforeShowEvent event) {
        studentsDl.setParameter("clazz", 0);
        studentsDl.load();
    }

    @Subscribe("clazzesTable")
    public void onClazzesTableSelection(Table.SelectionEvent<Clazz> event) {
        Set<Clazz> entities = event.getSelected();
        Clazz entity = entities.iterator().next();
        clazz = entity.getId();
        studentsDl.setParameter("clazz", clazz);
        studentsDl.load();
    }


}
