package com.haulmont.sample.petclinic.web.screens.area;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.Area;

@UiController("petclinic_Area.browse")
@UiDescriptor("area-browse.xml")
@LookupComponent("table")
@LoadDataBeforeShow
public class AreaBrowse extends MasterDetailScreen<Area> {
}