package com.haulmont.sample.petclinic.web.screens.layout;

import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.gui.ScreenBuilders;
import com.haulmont.cuba.gui.builders.AfterScreenCloseEvent;
import com.haulmont.cuba.gui.builders.ScreenClassBuilder;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.Area;
import com.haulmont.sample.petclinic.service.AreaService;
import com.haulmont.sample.petclinic.web.screens.imgcrop.ImageCropWindow;
import com.haulmont.sample.petclinic.web.screens.imgcrop.ImageCropWindowOptions;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.util.*;
import java.util.function.Consumer;

@UiController("petclinic_AreaScreen")
@UiDescriptor("area-screen.xml")
public class AreaScreen extends Screen {

    @Inject
    private LookupField provinceLookupField;
    @Inject
    private LookupField cityLookupField;
    @Inject
    private LookupField countyLookupField;
    @Inject
    private Label<String> label;
    @Inject
    private AreaService areaService;

    private AreaScreenOptions options;

    @Subscribe
    public void onInit(InitEvent event) {
        // 初始值设置值
        ScreenOptions options = event.getOptions();
        if(options instanceof AreaScreenOptions){
            this.options= (AreaScreenOptions) options;
        }
        if (Objects.isNull(this.options.getResult())) {
            // 设置新的数据
            provinceLookupField.setOptionsMap(getDataSource(0));
            if (!Objects.isNull(provinceLookupField.getValue())) {
                // 如果省份字段有值，使city字段可编辑
                cityLookupField.setEditable(true);
                if (!Objects.isNull(cityLookupField.getValue())) {
                    // 如果city字段有值，使县区字段可编辑
                    countyLookupField.setEditable(true);
                }
            }
        }
    }

    @Subscribe("okButton")
    public void onOkButtonClick(Button.ClickEvent event) {
        this.options.setResult(label.getValue());
        this.close(WINDOW_COMMIT_AND_CLOSE_ACTION);
    }

    @Subscribe("cancelButton")
    public void onCancelButtonClick(Button.ClickEvent event) {
        this.closeWithDefaultAction();
    }

    @Subscribe("provinceLookupField")
    public void onProvinceLookupFieldValueChange(HasValue.ValueChangeEvent event) {
        // 设置City数据源
        if (!cityLookupField.isEditable()) {
            cityLookupField.setEditable(true);
        }
        if (!Objects.isNull(event.getValue())) {
            cityLookupField.setOptionsMap(getDataSource(Integer.parseInt(event.getValue().toString())));
        }
        // 重置Label
        if (!Objects.isNull(event.getValue())) {
            label.setValue(getAreaValue(event.getValue().toString()));
        } else {
            if (cityLookupField.isEditable()) {
                if (!Objects.isNull(cityLookupField.getValue())) {
                    cityLookupField.setValue(null);
                }
                cityLookupField.setEditable(false);
            }
            if (countyLookupField.isEditable()) {
                if (!Objects.isNull(countyLookupField.getValue())) {
                    countyLookupField.setValue(null);
                }
                countyLookupField.setEditable(false);
            }
            label.setValue(null);
        }
    }

    @Subscribe("cityLookupField")
    public void onCityLookupFieldValueChange(HasValue.ValueChangeEvent event) {
        // 设置City数据源
        if (!countyLookupField.isEditable()) {
            countyLookupField.setEditable(true);
        }
        if (!Objects.isNull(event.getValue())) {
            countyLookupField.setOptionsMap(getDataSource(Integer.parseInt(event.getValue().toString())));
        }
        // 重置Label
        if (!Objects.isNull(event.getValue())) {
            label.setValue(getAreaValue(event.getValue().toString()));
        } else {
            if (countyLookupField.isEditable()) {
                if (!Objects.isNull(countyLookupField.getValue())) {
                    countyLookupField.setValue(null);
                }
                countyLookupField.setEditable(false);
            }
            if (!Objects.isNull(provinceLookupField.getValue())) {
                label.setValue(getAreaValue(provinceLookupField.getValue().toString()));
            } else {
                label.setValue(null);
            }
        }
    }

    @Subscribe("countyLookupField")
    public void onCountyLookupFieldValueChange(HasValue.ValueChangeEvent event) {
        // 重置Label
        if (!Objects.isNull(event.getValue())) {
            label.setValue(getAreaValue(event.getValue().toString()));
        } else {
            if (!Objects.isNull(cityLookupField.getValue())) {
                label.setValue(getAreaValue(cityLookupField.getValue().toString()));
            } else {
                if (!Objects.isNull(provinceLookupField.getValue())) {
                    label.setValue(getAreaValue(provinceLookupField.getValue().toString()));
                } else {
                    label.setValue(null);
                }
            }
        }
    }


    private Map<String, Integer> getDataSource(Integer code) {
        List<Area> areas = areaService.findAreaByParentCode(code);
        Map<String, Integer> map = new LinkedHashMap<>();
        for (Area area : areas) {
            map.put(area.getValue(), area.getCode());
        }
        return map;
    }

    private String getAreaValue(String code) {
        return AreaData.getArea(code);
    }

    public static void showAsDialog(FrameOwner origin, AreaScreenOptions options, Consumer<AfterScreenCloseEvent<AreaScreen>> closeEventConsumer){
        ScreenBuilders screenBuilders = AppBeans.get(ScreenBuilders.class);
        ScreenClassBuilder<AreaScreen> screenBuilder = screenBuilders.screen(origin)
                .withScreenClass(AreaScreen.class)
                .withLaunchMode(OpenMode.DIALOG)
                .withOptions(options);
        if(closeEventConsumer!=null){
            screenBuilder.withAfterCloseListener(closeEventConsumer);
        }
        AreaScreen screen = screenBuilder.build();
        screen.show();
    }

}
