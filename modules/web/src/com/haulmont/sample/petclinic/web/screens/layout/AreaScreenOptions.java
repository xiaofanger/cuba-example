package com.haulmont.sample.petclinic.web.screens.layout;

import com.haulmont.cuba.gui.screen.ScreenOptions;
import com.haulmont.sample.petclinic.web.toolkit.ui.imgcrop.ImgCropServerComponent;

import java.io.File;

/**
 * @author LiFan
 * @date 2019/12/16 11:44
 */
public class AreaScreenOptions implements ScreenOptions {

    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

//    public String windowWidth="800px";
//    public String windowHeight="600px";

}
