package com.haulmont.sample.petclinic.web.screens.subject;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Subject;

@UiController("petclinic_Subject.browse")
@UiDescriptor("subject-browse.xml")
@LookupComponent("subjectsTable")
@LoadDataBeforeShow
public class SubjectBrowse extends StandardLookup<Subject> {
}
