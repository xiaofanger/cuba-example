package com.haulmont.sample.petclinic.web.screens.school;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.School;

@UiController("petclinic_School.browse")
@UiDescriptor("school-browse.xml")
@LookupComponent("schoolsTable")
@LoadDataBeforeShow
public class SchoolBrowse extends StandardLookup<School> {
}
