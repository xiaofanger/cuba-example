package com.haulmont.sample.petclinic.web.screens.student;

import com.haulmont.cuba.gui.Route;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.Student;

@UiController("petclinic_Student.edit")
@UiDescriptor("student-edit.xml")
@EditedEntityContainer("studentDc")
@LoadDataBeforeShow
@Route(path = "student/edit", parentPrefix="student")
public class StudentEdit extends StandardEditor<Student> {
}
