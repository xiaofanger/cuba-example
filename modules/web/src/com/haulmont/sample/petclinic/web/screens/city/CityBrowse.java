package com.haulmont.sample.petclinic.web.screens.city;

import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Facet;
import com.haulmont.cuba.gui.components.Timer;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.City;

import javax.inject.Inject;

@UiController("petclinic_City.browse")
@UiDescriptor("city-browse.xml")
@LookupComponent("citiesTable")
@LoadDataBeforeShow
public class CityBrowse extends StandardLookup<City> {

    @Inject
    private Notifications notifications;

    public void onTimerClick(Timer source) {
        notifications.create().withDescription("定时器运行").show();
    }
}
