package com.haulmont.sample.petclinic.web.screens.school;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.School;

@UiController("petclinic_School.edit")
@UiDescriptor("school-edit.xml")
@EditedEntityContainer("schoolDc")
@LoadDataBeforeShow
public class SchoolEdit extends StandardEditor<School> {
}
