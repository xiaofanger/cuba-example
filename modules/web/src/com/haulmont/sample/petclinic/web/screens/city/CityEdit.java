package com.haulmont.sample.petclinic.web.screens.city;

import com.haulmont.cuba.gui.screen.*;
import com.haulmont.sample.petclinic.entity.school.City;

@UiController("petclinic_City.edit")
@UiDescriptor("city-edit.xml")
@EditedEntityContainer("cityDc")
@LoadDataBeforeShow
public class CityEdit extends StandardEditor<City> {
}
