package com.haulmont.sample.petclinic.web.screens.layout;

import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;

@UiController("petclinic_LayoutScreen")
@UiDescriptor("Layout-screen.xml")
public class LayoutScreen extends Screen {

    @Inject
    private PickerField areaPickerField;
    @Inject
    private Form form;
    @Inject
    private Label<String> label;

    @Subscribe("areaPickerField.lookup")
    protected void onPickerFieldLookupActionPerformed(Action.ActionPerformedEvent event) {
        AreaScreenOptions options = new AreaScreenOptions();
        AreaScreen.showAsDialog(this, options, (cropWindowAfterScreenCloseEvent)->{
            if(cropWindowAfterScreenCloseEvent.getCloseAction().equals(WINDOW_DISCARD_AND_CLOSE_ACTION)){
                //close by  "Cancel" button
            }else if(cropWindowAfterScreenCloseEvent.getCloseAction().equals(WINDOW_COMMIT_AND_CLOSE_ACTION)){
                // close by "ok" button
                String result = options.getResult();
                System.out.println(result);
//                areaPickerField.setValue();
                label.setValue(result);
            }
        });

    }

    @Subscribe("okButton")
    public void onOkButtonClick(Button.ClickEvent event) {
        form.getComponents().forEach(component -> {
            System.out.println(component.getId());
        });
    }

    @Subscribe("cancelButton")
    public void onCancelButtonClick(Button.ClickEvent event) {
        this.closeWithDefaultAction();
    }



}
