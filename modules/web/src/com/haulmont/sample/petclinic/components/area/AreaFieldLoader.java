package com.haulmont.sample.petclinic.components.area;

import com.haulmont.cuba.gui.xml.layout.loaders.AbstractFieldLoader;
import com.haulmont.sample.petclinic.components.stepper.StepperField;

public class AreaFieldLoader extends AbstractFieldLoader<AreaField> {

    @Override
    public void createComponent() {
        resultComponent = factory.create(AreaField.NAME);
        loadId(resultComponent, element);
    }

    @Override
    public void loadComponent() {
        super.loadComponent();
    }
}
