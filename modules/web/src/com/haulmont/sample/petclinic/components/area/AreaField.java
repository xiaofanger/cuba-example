package com.haulmont.sample.petclinic.components.area;

import com.haulmont.bali.events.Subscription;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.data.ValueSource;
import com.haulmont.cuba.gui.screen.FrameOwner;
import com.haulmont.cuba.web.gui.components.*;
import com.haulmont.sample.petclinic.web.screens.layout.AreaScreen;
import com.haulmont.sample.petclinic.web.screens.layout.AreaScreenOptions;

import java.util.Collection;
import java.util.function.Consumer;

@CompositeDescriptor("area-component.xml")
public class AreaField
        extends CompositeComponent<HBoxLayout>
        implements Field<String>,
        CompositeWithCaption,
        CompositeWithHtmlCaption,
        CompositeWithHtmlDescription,
        CompositeWithIcon,
        CompositeWithContextHelp {

    public static final String NAME = "areaField";

    private TextField<String> valueField;
    private Button openBtn;

    public AreaField() {
        addCreateListener(this::onCreate);
    }

    private void onCreate(CreateEvent createEvent) {
        valueField = getInnerComponent("valueField");
        openBtn = getInnerComponent("openBtn");
        // todo
        openBtn.addClickListener(clickEvent -> {
            System.out.println("------>>>>>>>> 初始化   " + valueField.getValue());

//            AreaScreenOptions options = new AreaScreenOptions();
//            AreaScreen.showAsDialog(AreaScreen.class, options, (cropWindowAfterScreenCloseEvent)->{
//                if(cropWindowAfterScreenCloseEvent.getCloseAction().equals(FrameOwner.WINDOW_DISCARD_AND_CLOSE_ACTION)){
//                    // close by "Cancel" button
//                }else if(cropWindowAfterScreenCloseEvent.getCloseAction().equals(FrameOwner.WINDOW_COMMIT_AND_CLOSE_ACTION)){
//                    // close by "ok" button
//                    String result = options.getResult();
//                    valueField.setValue(result);
//                }
//            });

        });
    }

    @Override
    public boolean isRequired() {
        return valueField.isRequired();
    }

    @Override
    public void setRequired(boolean required) {
        valueField.setRequired(required);
        getComposition().setRequiredIndicatorVisible(required);
    }

    @Override
    public String getRequiredMessage() {
        return valueField.getRequiredMessage();
    }

    @Override
    public void setRequiredMessage(String msg) {
        valueField.setRequiredMessage(msg);
    }

    @Override
    public void addValidator(Consumer<? super String> validator) {
        valueField.addValidator(validator);
    }

    @Override
    public void removeValidator(Consumer<String> validator) {
        valueField.removeValidator(validator);
    }

    @Override
    public Collection<Consumer<String>> getValidators() {
        return valueField.getValidators();
    }

    @Override
    public boolean isEditable() {
        return valueField.isEditable();
    }

    @Override
    public void setEditable(boolean editable) {
        valueField.setEditable(editable);
        openBtn.setEnabled(editable);
    }

    @Override
    public String getValue() {
        return valueField.getValue();
    }

    @Override
    public void setValue(String value) {

    }

    @Override
    public Subscription addValueChangeListener(Consumer<ValueChangeEvent<String>> listener) {
        return null;
    }

    @Override
    public void removeValueChangeListener(Consumer<ValueChangeEvent<String>> listener) {

    }


    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void validate() throws ValidationException {

    }

    @Override
    public void setValueSource(ValueSource<String> valueSource) {

    }

    @Override
    public ValueSource<String> getValueSource() {
        return null;
    }
}
